A very simple JavaScript app I wrote to help me learn chessboard coordinates on my laptop.

[https://griseri.fr/chessboard](https://griseri.fr/chessboard)

# Usage

Move the cursor around the chessboard to see the square coordinates.

Press the **F** key (like *flip*) to toggle coordinates from white or black perspective

Hold the **Shift** to see coordinates of the same rank (row)

Hold the **Ctrl** to see coordinates of the same file (column)

Press the **B** key (like *bishop*) to toggle coordinates on the same diagonals

Press the **Q** key (like *quizz*) to answer questions and try to guess coordinates

Press the **Esc** key to cancel a question

# Building

Compile Riot components with:

    riot ./src/app.riot -o ./dist/app.js 

Riot CLI must be installed, with

    npm install -g @riotjs/cli
